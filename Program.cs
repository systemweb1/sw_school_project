﻿using ConsoleApp_School.Class;
using ConsoleApp_School.Model;

namespace console_app_school
{
    public class Program
    {
        private static RegisterSystem _registerSys = new RegisterSystem(new SchoolSystem(1)); //set new registrant limit

        public static void Main(String[] args)
        {
            _registerSys.InitEvent();
            while (true)
            {
                Console.Write("Select choice: 1.Register 2.Exit --> ");
                var choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        RegisterUser();
                        break;
                    case "2":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Invalid choice. Try again.");
                        break;
                }
            }
        }

        public static void RegisterUser()
        {
            Console.Write("Enter your username: ");
            string? UserNameInput = Console.ReadLine();

            Console.Write("Are you far from school? yes = far, no = near: ");
            bool FarFromSchool = Console.ReadLine()?.ToLower() == "yes";

            _registerSys.RegisterStudent(new Student(0, UserNameInput, FarFromSchool, false));
        }
    }
}