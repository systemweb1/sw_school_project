using ConsoleApp_School.Enum;
using ConsoleApp_School.Interface;
using ConsoleApp_School.Model;

namespace ConsoleApp_School.Class
{
    public class GarageSystem : IGarageSystem
    {
        private readonly List<Vehicles> _result = new List<Vehicles>();

        public GarageSystem() { }

        public void AddNewVehicle()
        {
            _result.Add(new Sedan());
            _result.Add(new Van());
            _result.Add(new Bus());
        }

        public List<Vehicles> GetAllVehicles()
        {
            return _result;
        }

        public string PurchaseNewVehicle(List<Student> StudentList)
        {
            string Vehicle = string.Empty;

            for (int i = 1; i <= StudentList.Count; i++)
            {
                if (i <= (int)CarSeats.Sedan)
                {
                    Vehicle = Car.Sedan.ToString();
                }
                else if (i <= (int)CarSeats.Van)
                {
                    Vehicle = Car.Van.ToString();
                }
                else if (i <= (int)CarSeats.Bus)
                {
                    Vehicle = Car.Bus.ToString();
                }
            }

            return Vehicle;
        }
    }
}