namespace ConsoleApp_School.Class
{
    public class Publisher
    {
        public event EventHandler<Message> MyEvent;

        public void RaiseEvent(string message)
        {
            if (MyEvent != null)
            {
                var args = new Message(message);
                MyEvent.Invoke(this, args);
            }
        }
    }
}