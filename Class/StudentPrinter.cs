using ConsoleApp_School.Model;

namespace ConsoleApp_School.Class
{
    public class StudentPrinter
    {
        public void PrintStudentData(IReadOnlyList<Student> studentList)
        {
            Console.WriteLine();
            Console.WriteLine("Student Data:");
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine("| ID\t| Name\t\t| Distance from school\t|");
            Console.WriteLine("---------------------------------------------------");

            foreach (var item in studentList)
            {
                string IsFar = item.FarFromSchool ? "Far" : "Near";
                Console.WriteLine($"| {item.StudentId}\t| {item.StudentName}\t\t| {IsFar}\t\t\t|");
            }

            Console.WriteLine("---------------------------------------------------");
        }
    }
}