using ConsoleApp_School.Model;

namespace ConsoleApp_School.Class
{
    public class SchoolSystem
    {
        private readonly GarageSystem _garageSys;
        private readonly List<Student> _student;
        private int _max = 0;

        //propoties
        public List<Student> StudentList => _student;
        public int Max => _max;

        public SchoolSystem(int MaxRegistrant)
        {
            _max = MaxRegistrant;
            _garageSys = new GarageSystem();
            _student = new List<Student>();
        }

        public bool AddStudent(Student student)
        {
            if (!CheckDuplicate(student) && _student.Count < _max) //not duplicate
            {
                _student.Add(student);
                int? NewStudentId = _student.Any() ? _student.Max(m => m.StudentId) + 1 : 1;

                student.SetStudentId(NewStudentId ?? 0);
                student.SetRegisterStatus(true);
                return true;
            }
            else
            {
                _student.Add(student);
                student.SetRegisterStatus(false);
                LoadStudentToVehicles(_max);
                return false;
            }
        }

        private bool CheckDuplicate(Student student)
        {
            return _student.Contains(student); //override in Student.cs
        }

        public void LoadStudentToVehicles(int MaxRegistrant)
        {
            _student.Where(w => !w.RegisterStatus && w.FarFromSchool);
            _student.ForEach(f =>
            {
                if (f.FarFromSchool || _student.Count() > MaxRegistrant)
                {
                    List<Vehicles> callVehicles = _garageSys.GetAllVehicles();
                    if (callVehicles.Count == 0)
                    {
                        _garageSys.AddNewVehicle();
                    }

                    f.SetCarType(_garageSys.PurchaseNewVehicle(_student));
                }
            });
        }
    }
}