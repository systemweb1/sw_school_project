namespace ConsoleApp_School.Class
{
    public class Message
    {
        public string MessageResult { get; }

        public Message(string MessageResult)
        {
            this.MessageResult = MessageResult;
        }
    }
    
    public class Subscriber
    {
        public void OnEventRaised(object sender, Message e)
        {
            Console.WriteLine($"{e.MessageResult}"); //show message
        }
    }
}