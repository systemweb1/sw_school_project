using ConsoleApp_School.Model;

namespace ConsoleApp_School.Class
{
    public class RegisterSystem
    {
        // Encapsulated Fields
        private readonly SchoolSystem _schoolSys;
        private readonly MessageAnnouncement _message;
        private readonly Publisher _publisher;
        private readonly Subscriber _subscriber;
        private readonly StudentPrinter _printer;

        public RegisterSystem(SchoolSystem school)
        {
            _schoolSys = school;
            _message = new MessageAnnouncement();
            _publisher = new Publisher();
            _subscriber = new Subscriber();
            _printer = new StudentPrinter();
        }

        public void InitEvent()
        {
            _publisher.MyEvent += _subscriber.OnEventRaised;
        }

        public void RegisterStudent(Student student)
        {
            bool RegistrationStatus = _schoolSys.AddStudent(student);

            UpdateMessage(RegistrationStatus);
            ShowMessage();
        }

        private void UpdateMessage(bool RegistrationStatus)
        {
            foreach (var student in _schoolSys.StudentList)
            {
                _message.SetMessage(RegistrationStatus
                    ? $"\nRegistration success\nWelcome {student.StudentId} {student.StudentName}\nCurrent registration {_schoolSys.StudentList.Count}/{_schoolSys.Max}"
                    : student.FarFromSchool
                        ? $"\nRegistration fail for {student.StudentName}\nmax registrant: {_schoolSys.Max}\nSend back by {student.CarType}"
                        : $"\nRegistration fail for {student.StudentName}\nmax registrant: {_schoolSys.Max}");
            }
        }

        private void ShowMessage()
        {
            var successfulStudent = _schoolSys.StudentList.Where(w => w.RegisterStatus).ToList();
            _publisher.RaiseEvent(_message.ToString());
            _printer.PrintStudentData(successfulStudent);
        }
    }
}