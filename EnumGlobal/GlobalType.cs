namespace ConsoleApp_School.Enum
{
    public enum Car
    {
        Sedan,
        Van,
        Bus
    }

    public enum CarSeats
    {
        Sedan = 4,
        Van = 13,
        Bus = 25
    }
}