using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsoleApp_School.Enum;

namespace ConsoleApp_School
{
    public interface IVehicles
    {
        CarSeats Carry { get; }
        Car CarType { get; }
    }
}