using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsoleApp_School.Model;

namespace ConsoleApp_School.Interface
{
    public interface IGarageSystem
    {
        void AddNewVehicle();
        List<Vehicles> GetAllVehicles();
    }
}