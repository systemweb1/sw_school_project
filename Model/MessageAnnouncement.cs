namespace ConsoleApp_School.Model
{
    public class MessageAnnouncement
    {
        public string? ResultMessage { get; private set; }
        
        public MessageAnnouncement() { }

        public void SetMessage(string ResultMessage) { this.ResultMessage = ResultMessage; }

        public override string? ToString()
        {
            return this.ResultMessage;
        }
    }
}