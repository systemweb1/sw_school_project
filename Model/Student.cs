namespace ConsoleApp_School.Model
{
    public class Student
    {
        public int? StudentId { get; private set; }
        public string? StudentName { get; private set; } = string.Empty;
        public bool FarFromSchool { get; private set; }
        public bool RegisterStatus { get; private set; }
        public string? CarType { get; private set; }

        public Student(int? id, string? studentName, bool farFromSchool, bool registerStatus)
        {
            StudentId = id;
            StudentName = studentName;
            FarFromSchool = farFromSchool;
            RegisterStatus = registerStatus;
        }

        public void SetStudentId(int StudentId) => this.StudentId = StudentId;
        // public void SetStudentName(string StudentName) => this.StudentName = StudentName;
        public void SetRegisterStatus(bool RegisterStatus) => this.RegisterStatus = RegisterStatus;
        public void SetCarType(string CarType) => this.CarType = CarType;

        public override bool Equals(object? obj)
        {
            if (obj is not Student student)
            {
                return false;
            }

            return Equals(student);
        }

        public bool Equals(Student other)
        {
            if (other is null)
            {
                return false;
            }

            return StudentName == other.StudentName;
        }

        public override int GetHashCode() => StudentName?.GetHashCode() ?? 0;
    }
}