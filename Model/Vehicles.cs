using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConsoleApp_School.Enum;

namespace ConsoleApp_School.Model
{
    public abstract class Vehicles : IVehicles
    {
        public abstract CarSeats Carry { get; }

        public abstract Car CarType { get; }
    }

    public class Sedan : Vehicles
    {
        public override CarSeats Carry => CarSeats.Sedan;

        public override Car CarType => Car.Sedan;
    }

    public class Van : Vehicles
    {
        public override CarSeats Carry => CarSeats.Van;

        public override Car CarType => Car.Van;
    }

    public class Bus : Vehicles
    {
        public override CarSeats Carry => CarSeats.Bus;

        public override Car CarType => Car.Bus;
    }
}